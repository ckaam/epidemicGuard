import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/', //登录
      name: 'Login',
      component: () => import('@/views/Login'),
      meta: {
        title: '登录',
        requiresAuth: false,
      }
    },
    {
      path: '/Index', // 首页
      name: 'Index',
      component: () => import('@/views/Index'),
      meta: {
        title: '登录',
        requiresAuth: false,
      }
    },
    {
      path: '/History', // 历史记录
      name: 'History',
      component: () => import('@/views/History'),
      meta: {
        title: '历史记录',
        requiresAuth: false,
      }
    },
    {
      path: '/Detail', // 人员详情
      name: 'Detail',
      component: () => import('@/views/Detail'),
      meta: {
        title: '人员详情',
        requiresAuth: false,
      }
    },
    {
      path: '/HistoryDetail', // 人员详情（历史）
      name: 'HistoryDetail',
      component: () => import('@/views/HistoryDetail'),
      meta: {
        title: '人员详情',
        requiresAuth: false,
      }
    }
  ]
})
