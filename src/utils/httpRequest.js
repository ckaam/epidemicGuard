import Vue from 'vue'
import axios from 'axios'
import qs from 'qs'
import merge from 'lodash/merge'
import router from '../router'

const http = axios.create({
  timeout: 1000 * 30,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
})

/**
 * 请求拦截
 */
http.interceptors.request.use(config => {
    config.headers['token'] = window.localStorage.getItem('token'); //Vue.cookies.get('token');  请求头带上token 342edd525eb14f88acef091d09a0d9f3
      if(window.localStorage.getItem('token')==null){
        if(router.app._route.meta.requiresAuth){
            router.push('/')
        }
      }
  return config
}, error => {

  return Promise.reject(error)
})

http.interceptors.response.use(
  response => {
      if (response) {
          switch (response.data.code) {
              case 10020:
                  router.replace({
                      path: 'Login',
                      query: {redirect: router.currentRoute.fullPath}//登录成功后跳入浏览的当前页面
                  })
          }
      }
    return response;
  },
  error => {
    return Promise.reject(error)
  });


export default http
